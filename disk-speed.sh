#!/bin/bash
if [ -z "$1" ]; then
    echo -e "\nPlease call '$0 /path' to run this command!\n"
    exit 1
fi
file1="$1/input1.txt"
file2="$1/output1.txt"
file3="$file1-1"

echo start > $file1
echo start > $file2
echo create a new file to prevent caching of content....   takes some time
time tr -dc "A-Za-z0-9\n\r" < /dev/urandom | head -c 102400000 > $file1
echo copy $file1 to $file3 and remove $file1
time cp $file1 $file3
echo file created,  now going to do the reading and writing.  Takes some more time....
echo reading file: $file3 and filling: $file2
time while IFS= read -r line
do
echo -e "$line en wat extra tekst\n" >>$file2
done <"$file3"
echo reading/writing complete, removing the files now....

rm -f $file3
rm -f $file2
rm -f $file1