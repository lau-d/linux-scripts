#!/bin/csh -f 
#
########## cntlast: count logins, sort output by user
#...usage: cntlast [user]
#
if( "$1" != "" ) then	 ##logins of user $1 only
  last "$1" | more -ei
  exit
endif

set name = "$1" 
##-----------------------  else count all logins:

 #####...... too slow, with either awk or sed, skip it:
 #echo " --- counting all logins since ..."
 #echo " 	  " `last wtmp | awk '{print $3 $4 $5}'`
 #echo " 	  " `last wtmp | sed 's/wtmp begins//'`

echo " --- counting all logins and sorting ..."
last | awk '{print $1}' | sort | uniq -c | sort -nr
last wtmp	## to see since when
echo ""
echo -n "	--- To see logins of a user, enter username : "
set name = $<
if( "$name" != "" ) then
  last "$name" | more -ei
endif